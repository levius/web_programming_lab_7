package main.controller;


import main.*;
import main.model.Point;
import main.model.Service;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.transaction.*;
import java.io.Serializable;
import java.util.List;


public class Bean implements Serializable {

    private Service service = new Service();
    private List<Point> points;
    private double x;
    private double y;
    private double r;
    private double xCanvas;
    private double yCanvas;

    private boolean x1;

    public boolean isx1() {
        return x1;
    }

    public void setx1(boolean x1) {

        this.x1 = x1;
    }
    private boolean x2;

    public boolean isX2() {
        return x2;
    }

    public void setx2(boolean x2) {

        this.x2 = x2;
    }

    private boolean x3;

    public boolean isX3() {
        return x3;
    }

    public void setx3(boolean x3) {

        this.x3 = x3;
    }
    private boolean x4;

    public boolean isX4() {
        return x4;
    }

    public void setx4(boolean x4) {

        this.x4 = x4;
    }

    private boolean x5;

    public boolean isX5() {
        return x5;
    }

    public void setX5(boolean x5) {

        this.x5 = x5;
    }

    private boolean x6;

    public boolean isX6() {
        return x6;
    }

    public void setx6(boolean x6) {

        this.x6 = x6;
    }
    private boolean x7;

    public boolean isX7() {
        return x7;
    }

    public void setx7(boolean x7) {

        this.x7 = x7;
    }


    public double FINALX( ){
       boolean f[] = new boolean[] {x1 , x2 , x3,x4,x5,x6,x7 };
       int[] is = new int[]{-4, -3, -2, -1, 0, 1, 2};
       for (int i = 0; i < f.length; i++) {
           boolean b1 = f[i];
           if (b1) return is[i];
       }
       return 0;
    }

    public double getxCanvas() {
        return xCanvas;
    }

    public void setxCanvas(double xCanvas) {
        this.xCanvas = (double) Math.round(xCanvas * 1000d) / 1000d;
    }

    public double getyCanvas() {
        return yCanvas;
    }

    public void setyCanvas(double yCanvas) {
        this.yCanvas = (double) Math.round(yCanvas * 1000d) / 1000d;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }
    public void addPointFromCanvas() throws HeuristicRollbackException, RollbackException, NotSupportedException, HeuristicMixedException, SystemException {
        service.addPoint(new Point(xCanvas,yCanvas,r));
    }
    public void addPointFromForm() throws HeuristicRollbackException, RollbackException, NotSupportedException, HeuristicMixedException, SystemException {
        service.addPoint(new Point(FINALX(),y,r));
    }

    public List<Point> getPoints() {
        return service.getPoints();
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }
    public String tomain(){
        return null;
    }


}