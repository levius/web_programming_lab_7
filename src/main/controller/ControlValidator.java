package main.controller;


import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("ControlValidator")
public class ControlValidator implements Validator {


    @Override
    public void validate(FacesContext facesContext, UIComponent uiComponent, Object o) throws ValidatorException {

        try {

            if (Double.parseDouble(o.toString().replace(',', '.')) < -3 || Double.parseDouble(o.toString().replace(',', '.')) > 3) {
                FacesMessage facesMessage = new FacesMessage("Неверное значение Y");
                facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(facesMessage);
            }

        }
            catch (NullPointerException | NumberFormatException e) {
                FacesMessage facesMessage = new FacesMessage("Неверный Y");
                facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(facesMessage);
        }
    }

    @FacesValidator("ValidatorX")
    public static class ValidatorX implements Validator {
        static int counterChecked = 0;
        static int counterUnchecked = 0;

        @Override
        public void validate(FacesContext facesContext, UIComponent uiComponent, Object value) throws ValidatorException {
            if (value.equals(true)) counterChecked++;
            if (value.equals(false)) counterUnchecked++;
            System.out.printf("counterUnchecked=%d; counterChecked=%d\n", counterUnchecked, counterChecked);

            if ((counterUnchecked + counterChecked) < 7) return;
            if ((counterUnchecked + counterChecked) > 7) throw new ValidatorException(new FacesMessage("PONOS."));

            if (counterUnchecked != 1 && counterChecked != 1) {
                counterUnchecked = 0;
                counterChecked = 0;
                throw new ValidatorException(new FacesMessage("Выберите значение X. X может быть только один."));
            }
            counterUnchecked = 0;
            counterChecked = 0;
        }
    }
 }

