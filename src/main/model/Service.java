package main.model;


import main.model.Point;
import main.util.HibernateSession;

import javax.annotation.Resource;
import javax.persistence.*;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.transaction.*;
import javax.transaction.RollbackException;
import java.util.List;

public class Service {
    @Resource
    private UserTransaction userTransaction;
    public Service() {
    }
    private Query query;
    private List<Point> points;

    public List<Point> getPoints() {
        updatePoints();
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public void addPoint(Point point) throws HeuristicRollbackException, RollbackException, HeuristicMixedException, SystemException, NotSupportedException {

        EntityManager entityManager= HibernateSession.getEntityManager();
        FacesContext fCtx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
        String sessionId = session.getId();
        point.setSessionId(sessionId);

        entityManager.getTransaction().begin();
        entityManager.persist(point);
        entityManager.getTransaction().commit();

    }
    private void updatePoints(){
        FacesContext fCtx = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) fCtx.getExternalContext().getSession(false);
        String sessionID = session.getId();

        Query query = HibernateSession.getEntityManager().createQuery("select p from main.model.Point p WHERE p.sessionId =:id");
        query.setParameter("id",sessionID );
        points= query.getResultList();

    }
}