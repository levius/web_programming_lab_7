package main.model;


import javax.persistence.*;

@Entity
@Table(name = "Local")
public class Point {
    @Id
    @GeneratedValue
    @Column (name = "id")
    private int id;
    @Column(name = "x")
    private double x;
    @Column(name = "y")
    private double y;

    @Column(name = "r")
    private double r;
    @Column(name = "hit")
    private char hit;
    @Column(name = "sessionId")
    private String sessionId;

    public Point() {}

    public Point(double x, double y, double r) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.hit = isHitted(x, y, r) ? 'Y' : 'N';
    }


    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getShowPoint(int r) {
        return String.format("showPoint(%s,%s,%s)", x, y, this.r);
    }
    public int getId() {
        return id;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getR() {
        return r;
    }

    public char getHit() {
        return hit;
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getDrawPoint(int r) {
        return String.format("drawPoint(%s, %s, %s)", x, y, isHitted(x, y, r));
    }

    private static boolean isHitted(double x, double y, double r) {
        boolean triangle = ((y <= 0) && (y >= x - r) && (x >= 0) && (x <= y+r)) ;
        boolean kvadrat = ((y >= 0) && (y <= r) && (x >= 0) && (x <= r));
        boolean krug = ((y <= 0) && (y >= -r / 2) && (x <= 0) && (x >= -r/2) && (x*x + y*y <= r*r / 4));

        return triangle || kvadrat || krug;
    }

}
