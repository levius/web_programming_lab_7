package main.util;

import org.hibernate.SessionFactory;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

@ManagedBean
@ApplicationScoped
public class HibernateSession {

    @PersistenceContext(unitName = "")
    private EntityManager manager;
    private static SessionFactory sessionFactory;
    private static EntityManager entityManager;

    static {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("hibernate");
        entityManager = factory.createEntityManager();
    }

    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static EntityManager getEntityManager() {
        return entityManager;
    }

}
